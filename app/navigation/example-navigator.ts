import { createStackNavigator } from "react-navigation"
import { FirstExampleScreen } from "../screens/first-example-screen"
import { SecondExampleScreen } from "../screens/second-example-screen"
import { MapScreen } from "../screens/map-screen";

export const ExampleNavigator = createStackNavigator({
  firstExample: { screen: FirstExampleScreen },
  secondExample: { screen: SecondExampleScreen },
  mapScreen: { screen: MapScreen },
},
  {
    headerMode: "none",
  })
