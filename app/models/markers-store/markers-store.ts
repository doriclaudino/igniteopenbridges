import { SnapshotOut, types } from "mobx-state-tree"
import { toJS } from "mobx";

const CoordinateModel = types
  .model("Coordinates", {
    longitude: types.number,
    latitude: types.number,
  })

const MarkerModel = types
  .model("Marker", {
    coordinate: CoordinateModel
  })

/**
 * Model description here for TypeScript hints.
 */
export const MarkersStoreModel = types
  .model("MarkersStore")
  .props({
    markers: types.array(MarkerModel)
  })
  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({
    generateRandomMock() {
      console.log(`generateRandomMock`)
      //const coord: typeof CoordinatesModel.Type = { latitude: 1, longitude: 2 }
      self.markers.clear()
      self.markers.push({ coordinate: { latitude: 42.211, longitude: -71.164 } })
      self.markers.push({ coordinate: { latitude: 42.311, longitude: -71.264 } })
      self.markers.push({ coordinate: { latitude: 42.411, longitude: -71.364 } })
      console.log(toJS(self.markers))
    }
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
* Un-comment the following to omit model attributes from your snapshots (and from async storage).
* Useful for sensitive data like passwords, or transitive state like whether a modal is open.

* Note that you'll need to import `omit` from ramda, which is already included in the project!
*  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
*/

export type MarkersStore = typeof MarkersStoreModel.Type

export type TMarkersStoreModel = typeof MarkersStoreModel.Type
//type MarkersStoreType = Instance<typeof MarkersStoreModel>
//export interface MarkersStore extends MarkersStoreType { }
type MarkersStoreSnapshotType = SnapshotOut<typeof MarkersStoreModel>
export interface MarkersStoreSnapshot extends MarkersStoreSnapshotType { }
