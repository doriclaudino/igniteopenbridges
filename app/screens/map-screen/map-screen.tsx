import * as React from "react"
import { observer, inject } from "mobx-react"
import { View, ViewStyle, StyleSheet } from "react-native"
import { Screen } from "../../components/screen"
import { color } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import MapView, { PROVIDER_GOOGLE, Marker, MapEvent, Polygon, Polyline } from 'react-native-maps'
import { MarkersStore } from "../../models/markers-store";
import { customMapStyle } from "./map-screen.style";
import { observable, toJS } from "mobx";
import { Button } from "../../components/button";

export interface MapScreenProps extends NavigationScreenProps<{}> {
  markersStore: MarkersStore
}

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    flex: 1,
    flexDirection: 'column',
    ...StyleSheet.absoluteFillObject,
  },
});

@inject("markersStore")
@observer
export class MapScreen extends React.Component<MapScreenProps, {}> {
  @observable editing = null

  @observable handleOnLongPress = (e: MapEvent<{}>) => {
    this.handleCreateMarker(e)
  }

  switchToCreateLines() {
    this.handleOnLongPress = this.handleCreateLines
  }

  switchToCreateMarker() {
    this.handleOnLongPress = this.handleCreateMarker
  }

  undo() {
    if (this.editing && this.editing.coordinates) {
      if (this.editing.coordinates.length === 1)
        delete this.editing.coordinates
      else
        this.editing.coordinates.pop()
    } else {
      this.editing = null
      this.switchToCreateMarker()
    }
  }

  clearLines() {
    this.editing = {
      coordinate: this.editing.coordinate
    }
  }

  handleCreateMarker(e: MapEvent<{}>) {
    this.editing = {
      coordinate: e.nativeEvent.coordinate
    }
    this.switchToCreateLines()
  }

  handleCreateLines(e: MapEvent<{}>) {
    if (this.editing && this.editing.coordinates) {
      this.editing = {
        ...this.editing,
        coordinates: [...this.editing.coordinates, e.nativeEvent.coordinate],
      }
    } else {
      this.editing = {
        ...this.editing,
        id: this.editing ? this.editing.id++ : 0,
        coordinates: [e.nativeEvent.coordinate],
      }
    }
  }


  render() {
    return (
      <Screen style={ROOT} preset="fixedCenter">
        <View style={styles.container}>
          <MapView
            onLongPress={(event) => {
              this.handleOnLongPress(event)
            }}
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            customMapStyle={customMapStyle}
            initialRegion={{
              latitude: 42.411,
              longitude: -71.064,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            }}
            showsMyLocationButton={true}
            followsUserLocation={true}
            showsUserLocation={true}
          >
            {
              this.editing && this.editing.coordinate &&
              <Marker coordinate={this.editing.coordinate} />
            }

            {
              this.props.markersStore.markers.map((marker, index) => {
                return <Marker key={index}
                  {...marker}
                />
              })
            }
            {this.editing && this.editing.coordinates && this.editing.coordinates.length && (
              <Polyline
                key="editingPolyline"
                coordinates={this.editing.coordinates.map(coords => { return { ...coords } })}
                strokeColor="#000"
                fillColor="rgba(255,255,51,0.6)"
                strokeWidth={1}
              />
            )}

            {this.editing && this.editing.coordinates && this.editing.coordinates.length && (
              <Polygon
                key={this.editing.id}
                coordinates={this.editing.coordinates.map(coords => { return { ...coords } })}
                strokeColor="#000"
                fillColor="rgba(255,255,51,0.3)"
                strokeWidth={1}
              />)}

            {/*<MyLocationMapMarker />*/}
          </MapView>
          {
            this.editing &&
            <View
              style={{
                position: 'absolute',//use absolute position to show button on top of the map                
              }}
            >
              {this.editing.coordinates && (<Button text='clear lines' onPress={() => { this.clearLines() }} />)}
              <Button text='undo' onPress={() => { this.undo() }} />
            </View>
          }
        </View>
        {/*<Text preset="header" tx="mapScreen.header" />*/}
      </Screen>
    )
  }
}
